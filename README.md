# C# Control Structures Console Application

This console application demonstrates the use of various control structures in C#. It includes examples of `if-else` statements, `switch` statements, and different types of loops (`for`, `while`, `foreach`) along with `break` and `continue` statements.

## Features

- Demonstrates `if-else` statements with numerical input.
- Uses a `switch` statement for character input.
- Implements a `for` loop for iterative number printing with conditions.
- Uses a `while` loop for continuous user interaction until a specific command is entered.
- Demonstrates a `foreach` loop using a string array.
- Shows the use of `break` and `continue` within a loop.
- Incorporates a complex algorithm to identify prime numbers using nested `if` statements within a `for` loop.

## How to Run

1. Ensure you have a C# compiler or an IDE that supports C# (like Visual Studio).
3. Run the compiled executable from the command line or your IDE.

## How to Use

- Follow the on-screen prompts to interact with different control structures.
- Input numbers or characters as required by the prompt.
- Use the 'exit' command to terminate the `while` loop.

## Contributing

Contributions to this project are welcome. Feel free to fork the repository and submit pull requests.

## License

This project is open-source and available under the MIT License. See the LICENSE file for more details.
